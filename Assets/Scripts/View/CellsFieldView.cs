﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace View
{
    public class CellsFieldView : MonoBehaviour
    {
        private CellsField _cellsField;
        [SerializeField] private CellView _cellViewPrefab;
        [SerializeField] private Transform _instanceRoot;
        [SerializeField] private float _movingSpeed = 10;
        private Dictionary<Cell, CellView> _cellViews;

        public void Init(CellsField cellsField)
        {
            _cellsField = cellsField;
            _cellsField.OnCellCreated += OnCellCreated;
            _cellsField.OnCellDestroyed += OnCellDestroyed;
            _cellViews = new Dictionary<Cell, CellView>();
        }

        private void OnDestroy()
        {
            _cellsField.OnCellCreated -= OnCellCreated;
            _cellsField.OnCellDestroyed -= OnCellDestroyed;
        }

        private void OnCellDestroyed(Cell cell)
        {
            if (_cellViews.ContainsKey(cell))
            {
                Debug.Log($"{cell.Position} destroy");
            }

            Coroutine coroutine = StartCoroutine(DestroyDelay(_cellViews[cell]));
            _cellViews.Remove(cell);
        }

        IEnumerator DestroyDelay(CellView cellView)
        {
            yield return new WaitForSeconds(1 / _movingSpeed);
            Destroy(cellView.gameObject);
        }

        private void OnCellCreated(Cell cell)
        {
            CellView cellView = Instantiate(_cellViewPrefab, _instanceRoot);
            cellView.Init(cell, cell.Position, _movingSpeed);
            _cellViews.Add(cell, cellView);
        }
        
        
    }
}