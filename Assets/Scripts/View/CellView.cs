﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace View
{
    [RequireComponent(typeof(RectTransform))]
    public class CellView : MonoBehaviour
    {
        private RectTransform _rectTransform;
        [SerializeField] private TextMeshProUGUI _valueField;
        private Cell _cell;
        private Coroutine _updatePositionCoroutine;

        private Vector2 _targetPosition;

        private Vector2 _size;

        private float _movingSpeed;

        public void Init(Cell cell, Vector2Int position, float movingSpeed)
        {
            
            _cell = cell;
            _rectTransform = GetComponent<RectTransform>();
            
            _size = new Vector2(_rectTransform.rect.width, _rectTransform.rect.height);
            _rectTransform.localPosition = position * _size;
            
            _cell.OnValueChanged += UpdateValue;
            UpdateValue(_cell.Value);
            _cell.OnPositionChanged += UpdatePosition;
            UpdatePosition(_cell.Position);
            _movingSpeed = movingSpeed;
        }

        private void OnDestroy()
        {
            _cell.OnValueChanged -= UpdateValue;
            _cell.OnPositionChanged -= UpdatePosition;
        }

        private void UpdateValue(int value)
        {
            _valueField.text = value.ToString();
        }

        private void UpdatePosition(Vector2Int position)
        {
            if (_updatePositionCoroutine != null)
            {
                StopCoroutine(_updatePositionCoroutine);
            }
            _targetPosition = position * _size;

            _updatePositionCoroutine = StartCoroutine(UpdatePositionRoutine());
        }

        private IEnumerator UpdatePositionRoutine()
        {
            float time = 0;
            Vector2 oldPosition = _rectTransform.localPosition;
            while (time < 1)
            {
                _rectTransform.localPosition = Vector2.Lerp(oldPosition, _targetPosition, time);
                yield return null;
                time += Time.deltaTime * _movingSpeed;
            }

            _updatePositionCoroutine = null;
            _rectTransform.localPosition = _targetPosition;
        }
    }
}