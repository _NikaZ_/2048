using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using View;
using Random = UnityEngine.Random;

public class CellsField
{
    public event Action<Cell> OnCellCreated;
    public event Action<Cell> OnCellDestroyed;

    private readonly List<Cell> _cells;
    private readonly Vector2Int _fieldSize;
    private CellsFieldView _cellFieldView;

    public CellsField(CellsFieldView cellFieldView, int fieldWidth, int fieldHeight)
    {
        _cellFieldView = cellFieldView;
        _fieldSize = new Vector2Int(fieldWidth, fieldHeight);
        _cells = new List<Cell>();
        cellFieldView.Init(this);
        
        CreateCell();
        CreateCell();
    }

    private Vector2Int GetEmptyPosition()
    {
        var fieldPositions = new Vector2Int[_fieldSize.x * _fieldSize.y - _cells.Count];
        
        int counter = 0;
        for (int i = 0; i < _fieldSize.x; i++)
        {
            for (int j = 0; j < _fieldSize.y; j++)
            {
                Vector2Int position = new Vector2Int(i, j);
                if (HasCellsInPosition(position))
                {
                    continue;
                }
        
                fieldPositions[counter] = position;
                counter++;
            }
        }
        
        return fieldPositions[Random.Range(0, fieldPositions.Length)];
    }

    private bool HasCellsInPosition(Vector2Int pos)
    {
        foreach (Cell cell in _cells)
        {
            if (cell.Position == pos)
            {
                return true;
            }
        }

        return false;
    }

    public void Move(Vector2Int direction)
    {

        IOrderedEnumerable<Cell> orderedCells = OrderCells(direction);
        Cell[] canMoveCells = orderedCells.Where(cell => CanMoveCell(cell, direction)).ToArray();

        bool moved = false;
        
        while (canMoveCells.Length > 0)
        {
            canMoveCells = orderedCells.Where(cell => CanMoveCell(cell, direction)).ToArray();
            foreach (var cell in canMoveCells)
            {
                cell.Position += direction;
            }

            moved = true;
        }

        CombineCellsInSamePosition();

        if (moved)
        {
            CreateCell();
        }
    }

    private void CombineCellsInSamePosition()
    {
        for (int y = 0; y < _fieldSize.y; y++)
        {
            for (int x = 0; x < _fieldSize.x; x++)
            {
                var cells = _cells.Where(c => c.Position == new Vector2Int(x, y)).ToArray();
                if (cells.Length == 2)
                {
                    cells[0].Value *= 2;
                    _cells.Remove(cells[1]);
                    OnCellDestroyed?.Invoke(cells[1]);
                }
            }
        }
    }

    private IOrderedEnumerable<Cell> OrderCells(Vector2Int direction)
    {
        IOrderedEnumerable<Cell> orderedCells;
        if (direction == Vector2Int.left)
        {
            orderedCells = _cells.OrderBy(cell => cell.Position.x);
        }
        else if (direction == Vector2Int.up)
        {
            orderedCells = _cells.OrderBy(cell => cell.Position.y);
        }
        else if (direction == Vector2Int.right)
        {
            orderedCells = _cells.OrderByDescending(cell => cell.Position.x);
        }
        else
        {
            orderedCells = _cells.OrderByDescending(cell => cell.Position.y);
        }

        return orderedCells;
    }

    private void CreateCell()
    {
        if (_cells.Count == _fieldSize.y * _fieldSize.x)
        {
            return;
        }
        Vector2Int cellPosition = GetEmptyPosition();
        int chance = Random.Range(1, 101);
        
        int cellValue = 0;
        
        if (chance <= 10) 
            cellValue = 4;
        if (chance > 10)
            cellValue = 2;
        Cell cell = new Cell(cellPosition, cellValue);
        OnCellCreated?.Invoke(cell);
        _cells.Add(cell);
    }

    private bool CanMoveCell(Cell cell, Vector2Int direction)
    {
        Vector2Int newPosition = cell.Position + direction;
        if (newPosition.x >= _fieldSize.x || newPosition.x < 0 || newPosition.y >= _fieldSize.y || newPosition.y < 0)
        {
            return false;
        }

        Cell[] cellsOnNewPosition = _cells.Where(c => newPosition == c.Position).ToArray();
        if (cellsOnNewPosition.Length == 0 || cellsOnNewPosition.Length == 1 && cellsOnNewPosition[0].Value == cell.Value)
        {
            return true;
        }
        
        return false;
    }
    
}