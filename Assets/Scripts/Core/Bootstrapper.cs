﻿using System;
using UnityEngine;
using View;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private CellsFieldView _cellsFieldView;
        [SerializeField] private PlayerInput _playerInput;
        [SerializeField] private Vector2Int _fieldSize;
        
        private Game _game;
        private void Awake()
        {
            CellsField field = new CellsField(_cellsFieldView, _fieldSize.x, _fieldSize.y);
            Game game = new Game(field);
            _playerInput.Init(game);
            _game = game;
        }
    }
}