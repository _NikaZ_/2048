﻿using UnityEngine;

namespace Core
{
    public class Game
    {
        private CellsField _cellsField;
        public Game(CellsField cellsField)
        {
            _cellsField = cellsField;
        }

        public void Move(Vector2Int direction)
        {
            _cellsField.Move(direction);
        }
    }
}