using UnityEngine;

public class Cell
{
    public event System.Action<int> OnValueChanged;
    public event System.Action<Vector2Int> OnPositionChanged;
    
    private Vector2Int _position;
    private int _value;

    public Vector2Int Position
    {
        get => _position;
        set
        {
            _position = value;
            OnPositionChanged?.Invoke(value);
        }
    }

    public int Value
    {
        get => _value;
        set
        {
            _value = value;
            OnValueChanged?.Invoke(value);
        }
    }

    public Cell(Vector2Int position, int value)
    {
        _position = position;
        _value = value;
    }
}