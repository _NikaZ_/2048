﻿
using System;
using Core;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private Game _game;

    public void Init(Game game)
    {
        _game = game;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            _game.Move(Vector2Int.up);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            _game.Move(Vector2Int.down);

        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            _game.Move(Vector2Int.left);

        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            _game.Move(Vector2Int.right);

        }
        
    }
}